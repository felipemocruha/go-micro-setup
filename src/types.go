type Config struct {
	Host  string `yaml:"HOST"`
	ServerCertPath string `yaml:"SERVER_CERT_PATH"`
	ServerKeyPath  string `yaml:"SERVER_KEY_PATH"`
}
