import difflib

def test_setup_protobuf(model='resource', project='base'):
    want = '''syntax = "proto3";
package resource;

service Resource {
  rpc GetResources(ResourceFilter) returns (stream ResourceRequest) {}
  rpc CreateResource(ResourceRequest) returns (ResourceResponse) {}
  rpc UpdateResource(ResourceRequest) returns (ResourceResponse) {}
  rpc RemoveResource(ResourceFilter) returns (ResourceResponse) {}
  rpc GetResource(ResourceFilter) returns (ResourceRequest) {}
}

message ResourceRequest {
  //TO DO
}

message ResourceResponse {
  //TO DO
}

message ResourceFilter {
  //TO DO
}'''

    with open(f'{project}/{model}/{model}.proto') as f:
        got = f.read()

#    print([li for li in difflib.ndiff(want, got) if li[0] != ' '])
    assert want == got


def test_setup_makefile(model='resource', project='base'):
    want = '''VERSION=v0.1.0

proto:
	protoc -I resource/ resource/resource.proto --go_out=plugins=grpc:resource

server:
	cd server && go build -i -o resource

client:
	cd client && go build -i -o resource-client

cert_dev:
	certstrap --depot-path cert init --common-name "localhost"

compile:
	cd server && CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o resource .
	cd client && CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o resource-client .

docker:
	docker build -t base:$(VERSION) -f deploy/Dockerfile .'''

    with open(f'{project}/Makefile') as f:
        got = f.read()

    assert want == got


def test_setup_dockerfile(model='resource', project='base'):
    want = '''FROM alpine:3.8

RUN apk --update --no-cache add ca-certificates tzdata shadow \\
	&& cp /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime \\
    && echo "America/Sao_Paulo" > /etc/timezone \\
    && groupadd -r default \\
    && useradd --no-log-init -r -g default default

COPY build/base /usr/bin
USER default

CMD base'''

    with open(f'{project}/deploy/Dockerfile') as f:
        got = f.read()
    print([li for li in difflib.ndiff(want, got) if li[0] != ' '])
    print(want, got)
    assert want == got
