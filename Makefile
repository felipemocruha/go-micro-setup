PROJECT=base
MODEL=resource

clean:
	rm -rf $(PROJECT)

run:
	$(MAKE) clean && python main.py new $(PROJECT) -m $(MODEL)

test:
	nosetests
