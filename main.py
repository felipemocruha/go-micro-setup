'''
go-micro-setup - A poor engineered boilerplate killer

Usage:
  go-micro-setup new <project_name> -m <model>

Options:
  -h --help     Show this screen.
'''

import os
from docopt import docopt
from jinja2 import Environment, FileSystemLoader


def setup_protobuf(config, model):
    loader = FileSystemLoader(config['template_path'])
    template = loader.load(Environment(), 'proto_template.proto')

    try:
        os.mkdir(model)
        with open(f'{model}/{model}.proto', 'w') as f:
            f.write(template.render(model=model))

    except Exception as err:
        print(f'failed to setup protobuf: {str(err)}')


def setup_makefile(config, model, project):
    loader = FileSystemLoader(config['template_path'])
    template = loader.load(Environment(), 'Makefile')

    try:
        with open(f'Makefile', 'w') as f:
            f.write(template.render(model=model, project=project))

    except Exception as err:
        print(f'failed to setup makefile: {str(err)}')


def setup_dockerfile(config, model, project):
    loader = FileSystemLoader(config['template_path'])
    template = loader.load(Environment(), 'Dockerfile')

    try:
        os.mkdir('deploy')
        with open(f'deploy/Dockerfile', 'w') as f:
            f.write(template.render(model=model, project=project))

    except Exception as err:
        print(f'failed to setup dockerfile: {str(err)}')


def generate_pb_go():
    os.system('make proto')

def main(args):
    config = {
        'template_path': os.path.join(os.path.abspath('.'), 'src'),
    }

    model = args['<model>']
    project = args['<project_name>']

    try:
        os.mkdir(project)
        os.chdir(project)
        setup_protobuf(config, model)
        setup_makefile(config, model, project)
        setup_dockerfile(config, model, project)
        generate_pb_go()

    except Exception as err:
        print(f'failed to setup project: {str(err)}')


if __name__ == '__main__':
    args = docopt(__doc__, version='go-micro-setup v0.1.0')
    main(args)
